# K8S setup 

## with microk8S

### install
```
sudo snap install microk8s --classic
```

### Dashboard
microk8s enable ingress dns dashboard

run with & access at (http://localhost:8002/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#/pod?namespace=default)
```
kubectl proxy --address 0.0.0.0 --accept-hosts '.*' --port=8002
```


## with k3s

### install
```
curl -sfL https://get.k3s.io | sh -
sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
```

### kubectl
```
kubectl version || curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
kubectl version || sudo chmod +x ./kubectl
kubectl version || sudo mv ./kubectl /usr/local/bin/kubectl
```
### Install brew && kubectx
```
brew --version || /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

kubectx -h || brew install kubectx

sudo apt install fzf
```

## pull private Dockers
login with docker to get config
```
docker login registry.....
```
create secret for k8s
```
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/daan/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
```
