<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__);

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        '@PSR2' => true,
        'yoda_style' => false,
        'method_chaining_indentation' => true,
        'array_indentation' => true,
        'array_syntax' => [
            'syntax' => 'short',
        ],
        'not_operator_with_successor_space' => true,
        'binary_operator_spaces' => [
            'operators' => [
                '=' => 'single_space',
                '=>' => 'single_space',
            ],
        ],
        'ordered_imports' => [],
    ])
    ->setRiskyAllowed(true)
    ->setIndent('    ')
    ->setCacheFile(__DIR__.'/.php-cs.cache')
    ->setFinder($finder);
