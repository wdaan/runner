FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y unzip git nodejs curl nano php7.4 apache2 php7.4-mbstring php7.4-pdo php7.4-xml php7.4-curl php7.4-dom php7.4-fileinfo php7.4-imap php7.4-json php7.4-soap php7.4-mailparse php7.4-intl php7.4-gd php7.4-zip php7.4-mysql \
&& apt-get autoremove -y \
&& apt-get autoclean -y

#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer 

#set our application folder as an environment variable
ENV APP_HOME /var/www/html

#copy source files and run composer
WORKDIR /var/www/html
COPY --chown=www-data:www-data . /var/www/html

#install php dependencies
RUN composer install --no-dev --no-interaction --optimize-autoloader --ignore-platform-reqs

# npm i & build
#RUN curl https://www.npmjs.com/install.sh | sh \
#&& npm install --global cross-env
#RUN npm i && npm run prod && rm -r node_modules/

#change the web_root to laravel /var/www/html/public folder
COPY --chown=www-data:www-data .docker/000-default.conf /etc/apache2/sites-available/000-default.conf

# enable apache module rewrite
RUN a2enmod ssl headers rewrite
RUN a2dismod mpm_event && a2enmod mpm_prefork
RUN a2dissite default-ssl
RUN service apache2 restart

# copy start scripts
COPY .docker/start.sh /usr/local/bin/start

CMD ["/usr/local/bin/start"]
