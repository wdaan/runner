<?php

namespace App\Models;

use App\Models\Blocks\StartBlock;
use Exception;
use Illuminate\Support\Collection;
use JsonSerializable;

class Workspace implements JsonSerializable
{
    /**
     * @var Collection
     */
    public $blocks;

    /**
     * @var Collection
     */
    public $inputs;

    /**
     * @var Collection
     */
    public $response;

    public function __construct($data)
    {
        $this->response = new Collection();

        try {
            $workspace = (object) $data;
            $this->inputs = $this->setInputs($workspace->blocks);
            $this->blocks = $this->setBlocks($workspace->blocks);
        } catch (Exception $e) {
            throw new Exception('Invalid Json workspace');
        }
    }

    public function setBlocks($blocksData = [])
    {
        $blocks = new Collection();
        foreach ($blocksData as $blockData) {
            $blocks->push($this->createBlock($blockData));
        }

        return $blocks;
    }

    public function setInputs($blocksData = [])
    {
        return collect($blocksData)
            ->map(function ($block) {
                return collect($block)->only(['inputs', 'form']);
            })->flatten(2)
            ->map(function ($input) {
                if (! array_key_exists('label', $input)) {
                    $input['label'] = $input['id'];
                }

                return $input;
            })
            ->map('toObj')->keyBy('label');
    }

    /**
     * @param object|array $blockData
     *
     * @return Block
     */
    public function createBlock($blockData)
    {
        $blockData = is_array($blockData) ? (object) ($blockData) : $blockData;

        $className = 'App\Models\Blocks\\'.$blockData->type;

        return new $className($blockData, $this);
    }

    public function jsonSerialize()
    {
        return [
            'blocks' => $this->blocks->values()->jsonSerialize(),
        ];
    }

    public function run()
    {
        //Get start block
        $nextId = $this->getStartBlock()->childId;

        do {
            $block = $this->blocks->firstWhere('id', $nextId);
            $nextId = $block->childId;

            $response = $block->run();

            $this->response->push($response);
        } while ($nextId);

        return $this->response;
    }

    public function getStartBlock(): StartBlock
    {
        return $this->blocks->firstWhere('type', 'StartBlock');
    }
}
