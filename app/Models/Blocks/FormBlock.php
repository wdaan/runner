<?php

namespace App\Models\Blocks;

use App\Interfaces\iRunnable;
use App\Models\Workspace;

class FormBlock extends Block implements iRunnable
{
    /**
     * @var Collection
     */
    public $form;

    public function __construct($data, Workspace $workspace)
    {
        parent::__construct($data, $workspace);

        $this->form = collect($data->form ?? []);
    }

    public function run()
    {
        return $this->createResponse($this->form);
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize() + [
            'form' => $this->form->jsonSerialize(),
        ];
    }
}
