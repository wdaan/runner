<?php

namespace App\Models\Blocks;

use App\Interfaces\iRunnable;
use App\Models\Workspace;
use ArrayIterator;
use Exception;
use LimitIterator;

class LimitBlock extends Block implements iRunnable
{
    /**
     * @var array
     */
    private $list;

    /**
     * @var
     */
    private $limit;

    public function __construct($data, Workspace $workspace)
    {
        parent::__construct($data, $workspace);

        $this->limit = $this->sanitizeLimit($data->limit);
        $this->list = $this->sanitizeList($data->list);
    }

    public function run()
    {
    }

    private function sanitizeLimit($limit)
    {
        if (is_numeric($limit)) {
            return (int) $limit;
        }

        throw new Exception('Invalid limit provided.');
    }

    private function sanitizeList($list)
    {
        if (is_array($list)) {
            return new ArrayIterator($list);
        }

        if (is_iterable($list)) {
            return $list;
        }

        return new ArrayIterator();
    }

    public function getIterator()
    {
        return new LimitIterator($this->list, 0, $this->limit);
    }

    public function count()
    {
        return iterator_count($this->getIterator());
    }

    public function jsonSerialize()
    {
        return iterator_to_array($this->getIterator());
    }
}
