<?php

namespace App\Models\Blocks;

use App\Models\Workspace;
use JsonSerializable;

class Block implements JsonSerializable
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $type;

    /**
     * @var Collection
     */
    protected $inputs;
    /**
     * @var Collection
     */
    protected $settings;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $displayName;
    /**
     * @var bool
     */
    protected $disabled;

    /**
     * @var string|null
     */
    protected $loopBlockId;

    public function __construct($data, Workspace $workspace)
    {
        $this->workspace = $workspace;
        $this->id = $data->id ?? guid();
        $this->childId = $data->childId ?? null;
        $this->type = $data->type ?? class_basename($this);
        $this->inputs = collect($data->inputs ?? [])->map('toObj')->keyBy('id');
        $this->settings = collect($data->settings ?? [])->map('toObj')->keyBy('id');
        $this->name = $data->name ?? null;
        $this->displayName = $data->displayName ?? null;
        $this->disabled = $data->disabled ?? false;
        $this->loopBlockId = $data->loopBlockId ?? null;

        return $data;
    }

    public function createResponse($output = null, $error = null)
    {
        return [$this->id => array_filter(['output' => $output, 'error' => $error])];
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'childId' => $this->childId,
            'type' => $this->type,
            'inputs' => $this->inputs->values()->jsonSerialize(),
            'settings' => $this->settings->values()->jsonSerialize(),
            'name' => $this->name,
            'displayName' => $this->displayName,
            'disabled' => $this->disabled,
            'loopBlockId' => $this->loopBlockId,
        ];
    }
}
