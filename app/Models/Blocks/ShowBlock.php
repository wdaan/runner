<?php

namespace App\Models\Blocks;

use App\Interfaces\iRunnable;
use App\Models\Workspace;
use App\Traits\HasInputVariable;

class ShowBlock extends Block implements iRunnable
{
    use HasInputVariable;

    private $showVariable;

    public function __construct($data, Workspace $workspace)
    {
        parent::__construct($data, $workspace);

        $this->showVariable = $data->showVariable ?? $this->getInputVariable($workspace, 'input');
    }

    public function run()
    {
        return $this->createResponse($this->parseJsonPath($this->workspace, $this->showVariable));
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize() + [
            'showVariable' => $this->showVariable,
        ];
    }
}
