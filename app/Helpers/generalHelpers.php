<?php

use Webpatser\Uuid\Uuid;

/**
 * Creates a GUID.
 *
 * @return string
 */
function guid()
{
    return Uuid::generate()->string;
}

function toObj($array)
{
    return json_decode(json_encode($array));
}
