<?php

namespace App\Interfaces;

interface iRunnable
{
    public function run();
}
