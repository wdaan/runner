<?php

namespace App\Traits;

use App\Models\Workspace;
use Flow\JSONPath\JSONPath;

trait HasInputVariable
{
    protected function getInputVariable(Workspace $workspace, $name = 'input', $default = null)
    {
        return $this->removeUnicodeChars($this->parseInputField($workspace->inputs->get($name)->value ?? $default));
    }

    protected function getSettingVariable($name, $default = 'NULL')
    {
        return $this->removeUnicodeChars($this->parseInputField(object_get($this->settings->get($name), 'value') ?? $default));
    }

    protected function removeUnicodeChars($value = null)
    {
        if (is_string($value)) {
            return preg_replace('/[\x{200B}-\x{200D}]/u', '', $value);
        }

        return $value;
    }

    public function parseInputField($value, $allowJsonPath = true)
    {
        if (! $allowJsonPath) {
            return var_export($value, true);
        }

        return $value;
    }

    public function parseJsonPath(Workspace $workspace, $string)
    {
        if (is_null($string)) {
            return;
        }

        $jsonstore = new JSONPath(['inputs' => $workspace->inputs->toArray()]);

        //split strings into parts containing {}
        /*
        Hi test{$.inputs.'First Name'} {$.inputs.'Last Name'}!test

        matches =>

        Hi test
        {$.inputs.'First Name'}
        space
        {$.inputs.'Last Name'}
        !test

        */

        preg_match_all('/({(.*?)}|(.+?)(?={)|(.+))/', $string, $matches);

        $stringParts = $matches[1];

        foreach ($stringParts as $index => $selectorString) {
            //grab the part between {}
            preg_match('/(?<=\{).+?(?=\})/', $selectorString, $jsonPathMatch);

            if (isset($jsonPathMatch[0])) {
                $output = $jsonstore->find($jsonPathMatch[0])->data();

                if (isset($output[0])) {
                    $stringParts[$index] = $output[0]->value;
                }
            }
        }

        return implode('', $stringParts);
    }
}
