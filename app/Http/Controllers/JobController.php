<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessWorkspace;
use App\Workspace;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index(Request $request)
    {
        $w = new Workspace($request->input());
        $output = $w->run();

        return response()->json($output);
    }

    public function queue(Request $request)
    {
        $w = new Workspace($request->input());

        ProcessWorkspace::dispatchAfterResponse($w);

        return response()->json(['msg' => 'job send to queue']);
    }
}
