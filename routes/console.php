<?php

use Illuminate\Support\Facades\Artisan;
use App\Models\Workspace;
use App\Jobs\ProcessWorkspace;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('test', function () {
    $data = file_get_contents('tests/testBlends/blend_with_input_form.json');

    $w = new Workspace(json_decode($data, true));

    dump($w->run());
});

Artisan::command('q {times=1}', function ($times) {
    $data = file_get_contents('tests/testBlends/blend_with_input_form.json');

    $w = new Workspace(json_decode($data, true));

    for ($i = 0; $i < $times; ++$i) {
        ProcessWorkspace::dispatch($w);
    }
});
